# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

In this Installation guide, I will assume you have NodeJS, MongoDB, React and React Native installed on your local machine. If not, then please install them in order to run the task manager.

Please clone the file form bitbucket
```
git clone git@bitbucket.org:ryuatnorcal/taskmanager.git
```
Once installed, you will need to run mongoDB to be running before run others.
```
mongod
```
Once start mongoDB, you can run Express server at taskManager-server.
At default, Express server will run on port 3002
Please ensure that you run npm install to install node dependency before running server.
```
cd taskManager-server
npm install 
npm start
```
Next you will go in task-manager-client to start web application.
At default, React server will run on port 3001
Please ensure that you run npm install to install node dependency before running server.
```
cd ../ 
cd task-manager-client 
npm install 
npm start
```
Once running both express and react server, you will see web application at http://localhost:3001

### Mobile App ###
Please note that material ui is not fully support with iOS devices, it may not correctly apearing. 
Some of dependencies requires extra step to link togather. Please follow their github documentation before the following process.

Dependencies
react-native-material-ui
react-navigation
The mobile app is generated with non-quick generation method. Please see React Native get started page with Building Projects with Native Code

Please install all the dependencies 
```
cd ../ cd taskManagerMobile 
npm install
```
Once installed the node dependency, you will be able to run the app. Please ensure that xcode and/or android studio are ready to run on either emulator or device
For iOS
```
react-native run-ios
```
For Android
```
react-native run-android
```

Once app is running and decided to use network, Please connect your mobile device to the same WiFi as your computer is connected. 
Please find your IP4v on your computer and enter the address as prompted
If you decided to skip the networking step, you will redirect to welcome page. The non network version of app will not save your projects on local device, however, while in the same session, your data will be stored in redux storage.

I hope the installation guide helps you install the web app and mobile app. 
If there any problem, please scroll all the way down to email form for contact me.
### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact