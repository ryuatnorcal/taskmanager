export const timestampToDate = (date) =>{
  if(date){
    let months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
    if(typeof date == 'string'){
      date = new Date(date);
    }
    return date.getDate() + " " + months[date.getMonth()]  + " " + date.getFullYear();
  }
  else{
    return "TBA";
  }

}
