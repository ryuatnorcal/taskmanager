import React, { Component } from 'react';

import logo from './logo.svg';
import './App.css';

import {connect} from 'react-redux';

import Navbar from './components/navbar';
import Project from './components/project';
import GlobalStyle from './stylesheet/global.css';
import {Grid,Col, Row} from 'react-bootstrap';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {themeSettingInit} from './middlewares/dispachers';
import ProjectDetail from './components/project-detail';
import Form from './components/project-form';


class App extends Component {


  contentDispacher = () => {
    if(this.props.themeSetting){
      if(this.props.themeSetting.isProjectList){
        return(<Project />);
      }
      else if(this.props.themeSetting.isProjectDetail){
        return(<ProjectDetail />);
      }
      else if(this.props.themeSetting.isProjectForm){
        return(<Form />);
      }
    }
    else{
      themeSettingInit();
    }

  }

  render() {
    return (
      <MuiThemeProvider className="root">
        <Navbar />
        <Grid>
          <Row>
            <Col xs={12}  className="main">
              {this.contentDispacher()}
            </Col>
          </Row>
        </Grid>
      </MuiThemeProvider>
    );
  }
}

function mapStateToProps(state){
  return {
    themeSetting: state.themeSetting
  }
}



export default connect(mapStateToProps)(App);
