import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {selectProject} from '../actions/index';
import {removeTask} from '../middlewares/dispachers';
import {Col, Row, Table} from 'react-bootstrap';

class Tasks extends Component {
  tasks = () => {

    if(this.props.activeProject.tasks.length > 0){
      return this.props.activeProject.tasks.map((task)=>{
        return(
            <tr key={task.task}>
              <td>{task.task}</td>
              <td className="col-md-1"><button className="btn btn-info" onClick={(e)=> this.deleteTask(task.task,e)}>X</button></td>
            </tr>
          )
      });
    }
    else{
      return(<h3>There is no task at moment</h3>);
    }
  }
  deleteTask = (task, e)=>{
    e.preventDefault();
    removeTask(task);
  }
  render() {
    return (
      <section className="Tasks">
        <Row>
          <Col ms={12}>
            <Table>
              <thead>
                <tr>
                  <th>Task</th>
                  <th className="col-md-1">Done</th>
                </tr>
              </thead>
              <tbody>
                {this.tasks()}
              </tbody>
            </Table>
          </Col>
        </Row>
      </section>
    );
  }
}
function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}

export default connect(mapStateToProps)(Tasks);
