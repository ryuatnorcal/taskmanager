import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {selectProject} from '../actions/index';
import {addCommentToProject} from '../middlewares/dispachers';
import {FormGroup,ControlLabel,FormControl} from 'react-bootstrap';
import RaisedButton from 'material-ui/RaisedButton';
import uuid from 'uuid/v4';
class CommentForm extends Component {
  constructor(){
    super();
    this.state = {};
  }
  handleUpdate = (e) => {
    e.preventDefault();
    this.setState({
      comment: e.target.value
    })
  }

  submit = (e) =>{
    e.preventDefault();
    var now = new Date();
    var data = {
      comment: this.state.comment,
      id:uuid(),
      time: now
    }
    addCommentToProject(data);
  }

  render() {
    return (
      <section className="CommentForm">
        <FormGroup controlId="NewTask">
          <FormControl
            componentClass="textarea"
            name="comment"
            onChange={(e)=>this.handleUpdate(e)} />
        </FormGroup>
        <button className="btn btn-info" onClick={(e) => this.submit(e)}>Add Comment</button>
      </section>
    );
  }
}
function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}

export default connect(mapStateToProps)(CommentForm);
