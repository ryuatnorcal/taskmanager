import React, { Component } from 'react';
import {Table} from 'react-bootstrap';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {selectProject} from '../actions/index';
import {projectInit,deleteProject,selectedProject,changeTheme,unselectProject} from '../middlewares/dispachers';
import {timestampToDate} from '../tools/tools';
import FontAwesome from 'react-fontawesome';
class Project extends Component {

  componentWillMount = () => {
    projectInit();
  }

  removeProject = (id,e) => {
    e.preventDefault();
    deleteProject(id);
  }
  detailProject = (data,e) => {
    e.preventDefault();
    selectedProject(data);
    changeTheme("SHOW_PROJECT_DETAIL");
  }
  createNewProject = (arg,e) => {
    e.preventDefault();
    unselectProject();
    changeTheme(arg);
  }

  project = ()=>{

    let d = this.props.projects;
    return d.map((pj) => {
      return(
        <tr key={pj.id}>
          <td onClick={(e)=>this.detailProject(pj,e)}>{pj.name}</td>
          <td onClick={(e)=>this.detailProject(pj,e)}>{timestampToDate(pj.startAt)}</td>
          <td onClick={(e)=>this.detailProject(pj,e)}>{timestampToDate(pj.dueAt)}</td>
          <td onClick={(e)=>this.detailProject(pj,e)}>{timestampToDate(pj.endAt)}</td>
          <td onClick={(e)=>this.detailProject(pj,e)}>{this.props.status[pj.status].option}</td>
          <td className="col-sm-1"><button className="btn btn-transparent" onClick={(e)=>this.removeProject(pj.id,e)}><FontAwesome name="trash" /></button></td>
        </tr>
      );
    });
  }

  projectListView = () => {
    let d = this.props.projects;
    if(d.length > 0){
      return (
        <Table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Start</th>
              <th>Due</th>
              <th>End</th>
              <th>Status</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {this.project()}
            <button className="btn btn-info" onClick={(e)=>this.createNewProject("CREATE_NEW_PROJECT",e)}>Create New Project</button>
          </tbody>
        </Table>
      );
    }
    else{
      return (
        <section>
          <h3>Wooot! There is no Project!</h3>
          <button className="btn btn-info" onClick={(e)=>this.createNewProject("CREATE_NEW_PROJECT",e)}>Create New Project</button>
        </section>
      );
    }

  }

  render() {
    return (
      <section className="Project">
        <h2>Project Queues</h2>
          {this.projectListView()}
      </section>
    );


  }
}
function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
    themeSetting: state.themeSetting,
    status: state.status
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}

export default connect(mapStateToProps)(Project);
