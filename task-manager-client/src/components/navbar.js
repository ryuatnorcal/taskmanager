import React, { Component } from 'react';
import navStyle from '../stylesheet/navbar.css';
import FontAwesome from 'react-fontawesome';
import {ListItem} from 'material-ui/List';
import {changeTheme} from '../middlewares/dispachers';
class Navbar extends Component {
  constructor(){
    super();
    this.state = {};
  }

  componentDidMount = () =>{
    this.setState({
      menuIcon: "bars",
      isMenuOpen: false,
      slideMenu: "slideOut"
    })
  }

  toggleMenu = (e) => {
    console.log("toggleMneu")
    e.preventDefault();
    if(this.state.isMenuOpen){
      this.setState({
          menuIcon:"bars",
          slideMenu: "slideOut"
      })
    }
    else{
      this.setState({
          menuIcon:"times",
          slideMenu: "slideIn"
      })

    }
    this.setState({
      isMenuOpen: !this.state.isMenuOpen
    })

  }

  changeView = (arg,e) =>{
    e.preventDefault();
    changeTheme(arg);
    this.toggleMenu(e);
  }

  render() {
    return (
      <section className="NavWrapper">
        <section className="Navbar">
          <button className="btn btn-transparent" onClick={(e)=>this.toggleMenu(e)}>
            <FontAwesome name={this.state.menuIcon} />
          </button>
        </section>
        <section className={'sideNav ' + this.state.slideMenu}>
          <ListItem primaryText="Dashboard" onClick={(e)=>this.changeView("SET_AS_DEFAULT",e)} leftIcon={<FontAwesome name="inbox" />} />
          <ListItem primaryText="Projects" onClick={(e)=>this.changeView("SET_AS_DEFAULT",e)} leftIcon={<FontAwesome name="window-restore" />} />
          <ListItem primaryText="New Projects" onClick={(e)=>this.changeView("CREATE_NEW_PROJECT",e)} leftIcon={<FontAwesome name="window-maximize" />} />
        </section>
      </section>
    );
  }
}

export default Navbar;
