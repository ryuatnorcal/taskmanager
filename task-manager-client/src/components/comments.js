import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {selectProject} from '../actions/index';
import {removeComment} from '../middlewares/dispachers';
import {Col, Row, Table} from 'react-bootstrap';
import {Card, CardActions, CardHeader, CardText} from 'material-ui/Card';
import FontAwesome from 'react-fontawesome';
import {timestampToDate} from '../tools/tools';
import style from '../stylesheet/comment.css';
class Comments extends Component {
  comments = () => {
    if(this.props.activeProject.comments.length > 0){
      return this.props.activeProject.comments.map((data)=>{
          return(
            <Card key={data.id} className="cards">
              <CardText expandable={false}>
                <p>{data.comment}</p>
              </CardText>
              <CardActions>
                <Row>
                  <Col sm={11}>
                    <p className="date">Entered at {timestampToDate(data.time)}</p>
                  </Col>
                  <Col sm={1}>
                    <button className="btn btn-danger" onClick={(e) => this.deleteComent(data.comment,e)}><FontAwesome name="trash" /></button>
                  </Col>
                </Row>
              </CardActions>
            </Card>
          )
      });
    }
    else{
      return(<h3>There is no comment at the moment</h3>);
    }
  }
  deleteComent = (comment, e)=>{
    e.preventDefault();
    removeComment(comment);
  }

  render() {
    return (
      <section className="Comments">
        {this.comments()}
      </section>
    );
  }
}
function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}

export default connect(mapStateToProps)(Comments);
