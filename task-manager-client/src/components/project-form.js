import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {FormGroup, ControlLabel, FormControl,Col,Row,Grid} from 'react-bootstrap';
import {createProject,initializeStatus,updateProject,changeTheme} from '../middlewares/dispachers';
import {selectProject} from '../actions/index';
import {timestampToDate} from '../tools/tools';
import uuid from 'uuid-v4';
import TextField from 'material-ui/TextField';
class projectForm extends Component {
  constructor(){
    super();
    this.state ={};
  }
  componentWillMount(){
    initializeStatus();
    this.initializeState();

  }


  initializeState = () => {

    if(this.props.activeProject){
      this.setState({
        name: this.props.activeProject.name,
        detail: this.props.activeProject.detail,
        status: this.props.activeProject.status,
        dueAt: this.props.activeProject.dueAt,
        endAt: this.props.activeProject.endAt,
        starAt: this.props.activeProject.startAt,
        id:this.props.activeProject.id,
        header: "Edit " + this.props.activeProject.name
      });
    }
    else{
      let now = new Date();
      this.setState({
        name: " ",
        detail: " ",
        status: 0,
        dueAt: now,
        endAt: now,
        starAt: now,
        header: "Create New Project"
      });
    }

  }

  getSelect = () => {

    if(this.props.status){
      return this.props.status.map((select)=>{
        return(
          <option key={select.value} value={select.value}>{select.option}</option>
        )
      })
    }
  }
  handleUpdate = (id,e) => {
    switch(id){
      case "formControlsProject":
        this.setState({
          name:e.target.value
        })
        break;
      case "formControlsStartDate":
        this.setState({
          startAt:e.target.value
        })
        break;
      case "formControlsDueDate":
        this.setState({
          dueAt:e.target.value
        })
        break;
      case "formControlsEndDate":
        this.setState({
          endAt:e.target.value
        })
        break;
      case "formControlsDescription":
        this.setState({
          detail:e.target.value
        })
        break;
      case "formControlsSelect":
        this.setState({
          status:e.target.value
        })
      break;
    }
  }

  FieldGroup = (id, label,type,name, placeholder="",state) => {
    switch(id){
      case "formControlsProject":
      case "formControlsStartDate":
      case "formControlsDueDate":
      case "formControlsEndDate":

        return(
          <FormGroup controlId={id}>
            <ControlLabel>{label} </ControlLabel>
            <TextField
              type={type}
              placeholder={placeholder}
              name={name}
              value={state}
              fullWidth={true}
              onChange={(e)=>this.handleUpdate(id,e)} />
          </FormGroup>
        );
        break;
      case "formControlsDescription":
          return(
            <FormGroup controlId={id}>
              <ControlLabel>{label} </ControlLabel>
              <TextField
                componentClass={type}
                type={type}
                placeholder={placeholder}
                name={name}
                value={state}
                multiLine={true}
                fullWidth={true}
                onChange={(e)=>this.handleUpdate(id,e)} />
            </FormGroup>
          );
        break;

      case "formControlsSelect":
        return(
          <FormGroup controlId={id}>
            <ControlLabel>{label} </ControlLabel>
            <FormControl
              componentClass={type}
              placeholder={placeholder}
              value={state}
              onChange={(e)=>this.handleUpdate(id,e)} >
                {this.getSelect()}
            </FormControl>
          </FormGroup>
        )
        break;
    }

  }

  submit = () => {
    if(this.state.id){
      var data = {
        name: this.state.name,
        detail: this.state.detail,
        status: this.state.status,
        dueAt: new Date(this.state.dueAt),
        endAt: new Date(this.state.endAt),
        starAt: new Date(this.state.startAt),
        id:this.state.id
      }
      updateProject(data);
    }
    else{
      var data = {
        name: this.state.name,
        detail: this.state.detail,
        status: this.state.status,
        dueAt: new Date(this.state.dueAt),
        endAt: new Date(this.state.endAt),
        starAt: new Date(this.state.startAt),
        id:uuid()
      }
      createProject(data);
    }
    changeTheme("SET_AS_DEFAULT");
  }


  render() {
    let timestamp = new Date();
    let now = timestamp.getMonth()+1 + "/" + timestamp.getDate() + "/"+ timestamp.getYear();
    return (
      <section className="projectForm">
          <Row>
            <Col xs={12}>
              <h2>{this.state.header}</h2>
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              {this.FieldGroup("formControlsProject","Project","text","project","Enter Text",this.state.name)}
            </Col>
          </Row>
          <Row>
            <Col sm={4}>
              {this.FieldGroup("formControlsStartDate","Start","date",this.state.startAt,this.state.startAt)}
            </Col>
            <Col sm={4}>
              {this.FieldGroup("formControlsDueDate","Due","date",this.state.dueAt,this.state.dueAt)}
            </Col>
            <Col sm={4}>
              {this.FieldGroup("formControlsEndDate","End","date",this.state.endAt,this.state.endAt)}
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              {this.FieldGroup("formControlsDescription","Detail","textarea","project","Enter Text",this.state.detail)}
            </Col>
          </Row>
          <Row>
            <Col xs={4}>
              {this.FieldGroup("formControlsSelect","Status","select","project","0",this.state.status)}
            </Col>
          </Row>
          <Row>
            <Col xs={12}>
              <button className="btn btn-primary" onClick={this.submit}>Save</button>
            </Col>
          </Row>

      </section>
    );
  }
}
function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
    status: state.status
  }
}
function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}

export default connect(mapStateToProps)(projectForm);
