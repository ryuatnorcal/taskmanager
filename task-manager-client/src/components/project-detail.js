import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {selectProject} from '../actions/index';
import {projectInit,deleteProject,unselectProject} from '../middlewares/dispachers';
import {Col, Grid, Row} from 'react-bootstrap';
import Tasks from './tasks';
import TaskForm from './task-form';
import {timestampToDate} from '../tools/tools';
import FontAwesome from 'react-fontawesome';
import {changeTheme} from '../middlewares/dispachers';
import CommentForm from './comment-form';
import Comments from './comments';
import pdStyle from '../stylesheet/project-detail.css';
class ProjectDetail extends Component {

 removeProject(id,e){
   e.preventDefault();
   deleteProject(id);
   unselectProject();
 }

  detail = () => {
    if(this.props.activeProject != null){
      return (
        <section>
          <Row>
            <Col sm={8}>
              <h1>{this.props.activeProject.name.toUpperCase()}</h1>
              <p>{this.props.activeProject.detail}</p>
            </Col>
            <Col sm={2}>
              <button className="btn btn-transparent" onClick={(e)=>changeTheme("SET_AS_DEFAULT")}><FontAwesome name="reply"/></button>
              <button className="btn btn-transparent" onClick={(e)=>changeTheme("EDIT_PROJECT")}><FontAwesome name="edit" /></button>
              <button className="btn btn-transparent" onClick={(e)=>this.removeProject(this.props.activeProject.id,e)}><FontAwesome name="trash" /></button>
            </Col>
            <Col sm={2}>
              <label className={this.props.status[this.props.activeProject.status].cls}>{this.props.status[this.props.activeProject.status].option}</label>
            </Col>
          </Row>
          <Row>
            <Col sm={4}>
              <p>Start: {timestampToDate(this.props.activeProject.startAt)}</p>
            </Col>
            <Col sm={4}>
              <p>Due: {timestampToDate(this.props.activeProject.dueAt)}</p>
            </Col>
            <Col sm={4}>
              <p>End: {timestampToDate(this.props.activeProject.endAt)}</p>
            </Col>
          </Row>
          <Row>
            <Col sm={12}>
              <Comments />
              <CommentForm />
              <Tasks />
              <TaskForm />
            </Col>
          </Row>
        </section>
      )
    }

  }
  render() {
    return (
      <section className="ProjectDetail">
        {this.detail()}

      </section>
    );
  }
}
function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
    status: state.status
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}

export default connect(mapStateToProps)(ProjectDetail);
