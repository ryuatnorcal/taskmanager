import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {selectProject} from '../actions/index';
import {addTaskToProject} from '../middlewares/dispachers';
import {FormGroup,ControlLabel,FormControl} from 'react-bootstrap';
import RaisedButton from 'material-ui/RaisedButton';
import uuid from 'uuid/v4';
class TaskForm extends Component {
  constructor(){
    super();
    this.state = {};
  }
  handleUpdate = (e) => {
    e.preventDefault();
    this.setState({
      task: e.target.value
    })
  }

  submit = (e) =>{
    e.preventDefault();
    var data = {
      task: this.state.task,
      id:uuid()     
    }
    addTaskToProject(data);
  }

  render() {
    return (
      <section className="TaskForm">
        <FormGroup controlId="NewTask">
          <FormControl
            type="text"
            name="task"
            onChange={(e)=>this.handleUpdate(e)} />
        </FormGroup>
        <button className="btn btn-info" onClick={(e) => this.submit(e)}>Add Task</button>
      </section>
    );
  }
}
function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}

export default connect(mapStateToProps)(TaskForm);
