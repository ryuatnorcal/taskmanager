import {createStore,applyMiddleware} from 'redux';
import axios from 'axios';
import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import allReducers from '../reducers/index';

const middleware = applyMiddleware(promise(),thunk);
const store = createStore(allReducers,middleware);

export default store;
