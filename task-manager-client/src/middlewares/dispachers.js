import axios from 'axios';
import store from '../middlewares/store';

export const themeSettingInit = () => {
  store.dispatch((dispatch)=>{
    dispatch({type:"SETTING_INIT"});
  })
}

export const projectInit = ()=>{
  store.dispatch((dispatch)=>{
    dispatch({type:"FETCH_START"});
    axios.get("/api/projects").then((response)=>{
      dispatch({type:"RECIEVED_PROJECTS",payload:response.data});
    }).catch((err)=>{
      dispatch({type:"FAILED",payload:err});
    })
  });
}

export const createProject = (data)=>{
  store.dispatch((dispatch)=>{
    axios.post('/api/project',data).then((response)=>{
      dispatch({type:"ADD_PROJECT",payload:data});
    }).catch((err)=> {
      console.log("Err " + err);
    });
  });
}

export const updateProject = (data) => {
  store.dispatch((dispatch) => {
    axios.post('/api/project/update',data).then((respond)=>{
      dispatch({type:"UPDATE_PROJECT",payload:data});
    })
  })
}
export const deleteProject = (id) => {
  let data = store.getState();
  store.dispatch((dispatch)=>{
    axios.get('/api/project/delete/'+id).then((response)=>{
      data = removeById(data.projects,id);
      dispatch({type:"REMOVE_PROJECT",payload:data});
    }).catch((err)=>{
      console.log("Err " + err);
    })
  })
}

export const selectedProject = (data) =>{

  store.dispatch((dispatch)=>{
      let state = store.getState();
      dispatch({type:"SELECTED_PROJECT",payload:data});
      dispatch({type:"REDEFINE_PROJECTS",payload:state.projects});
  })
}
export const unselectProject = ()=>{
  store.dispatch((dispatch)=>{
    let state = store.getState();
    dispatch({type:"UNSELECT_PROJECT"});
    dispatch({type:"REDEFINE_PROJECTS",payload:state.projects});
  });
}

export const initializeStatus = ()=>{
  store.dispatch((dispatch)=>{
    let state = store.getState();
    dispatch({type:"INITIALIZE_STATUS",payload:state.activeProject});
    dispatch({type:"REDEFINE_PROJECTS",payload:state.projects});

  })
}


export const addTaskToProject = (arg)=>{

  store.dispatch((dispatch)=>{
    let state = store.getState();
    let data = {
      task: arg.task,
      id: state.activeProject.id
    }
    axios.post('/api/project/add/task',data).then((respond)=>{
      dispatch({type:"ADD_TASK",payload:data});
      dispatch({type:"REDEFINE_PROJECTS",payload:state.projects});
    })
  });
}
export const addCommentToProject = (arg) => {
  store.dispatch((dispatch)=>{
    let state = store.getState();
    let data = {
      comment: arg.comment,
      id: state.activeProject.id,
      time: arg.time
    }
    axios.post('/api/project/add/comment',data).then((respond)=>{
      dispatch({type:"ADD_COMMENT",payload:data});
      dispatch({type:"REDEFINE_PROJECTS",payload:state.projects});
    })
  });
}

export const removeComment = (arg)=>{
  store.dispatch((dispatch)=> {
    let state = store.getState();
    let data = {
      comment : arg,
      id: state.activeProject.id
    }
    axios.post('/api/project/remove/comment',data).then((respond)=>{
      dispatch({type:"REMOVE_COMMENT",payload:arg});
      dispatch({type:"REDEFINE_PROJECTS",payload:state.projects});
    })
  });
}

export const removeTask = (arg)=>{
  store.dispatch((dispatch)=> {
    let state = store.getState();
    let data = {
      task : arg,
      id: state.activeProject.id
    }
    axios.post('/api/project/remove/task',data).then((respond)=>{
      dispatch({type:"REMOVE_TASK",payload:arg});
      dispatch({type:"REDEFINE_PROJECTS",payload:state.projects});
    })
  });
}

export const changeTheme = (arg) => {
  store.dispatch((dispatch)=>{
    let state = store.getState();
    dispatch({type: arg,payload: state.themeSetting});
  });
}


function removeById(state,id){
  for(let i = 0; i < state.length; i++){
    if(state[i].id == id){
      state.splice(i,1);
    }
  }
  return state;
}
