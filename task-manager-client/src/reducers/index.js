import {combineReducers} from 'redux';
import UserReducer from './reducer-users';
import ActiveUserReducer from './reducer-active-user';
import ProjectReducer from './reducer-project';
import ActiveProject from './reducer-active-project';
import StatusReducer from './reducer-status';
import ThemeSettingReducer from './reducer-theme-setting';

const allReducers = combineReducers({
  users: UserReducer,
  activeUser: ActiveUserReducer,
  projects: ProjectReducer,
  activeProject:ActiveProject,
  status: StatusReducer,
  themeSetting: ThemeSettingReducer
});
export default allReducers;
