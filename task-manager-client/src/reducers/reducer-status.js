export default function(state=null,action){
  switch(action.type){
      case "INITIALIZE_STATUS":
      default:
        return [
          {
            value: 0,
            option: "New",
            cls: "new"
          },
          {
            value: 1,
            option: "In Progress",
            cls: "prog"
          },
          {
            value: 2,
            option: "Development",
            cls: "dev"
          },
          {
            value: 3,
            option: "Testing",
            cls: "test"
          },
          {
            value: 4,
            option: "Fixing",
            cls: "fix"
          },
          {
            value: 5,
            option: "Done"
          }
        ]
        break;

  }

}
