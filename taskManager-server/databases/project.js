var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var project = new Schema({
  id: String,
  name: String,
  detail: String,
  status: Number,
  startAt: Date,
  lastEdit: Date,
  dueAt: Date,
  endAt: Date,
  comments: Array,
  tasks: Array
});

// Compile model from schema
var projectModel = mongoose.model('projectModel', project );
