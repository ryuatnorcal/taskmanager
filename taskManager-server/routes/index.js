var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var project = require('../databases/project');
var Projects = mongoose.model('projectModel');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/delete/all/projects',function(req,res,next){
  Projects.find(function(err,data){
    if(data){
      console.log(data);
      for(let i = 0;i<data.length;i++){
        data[i].remove();
      }


    }
    res.status(200).end();
  })
})

module.exports = router;
