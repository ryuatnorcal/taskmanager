var express = require('express');
var mongoose = require('mongoose');
var router = express.Router();
var project = require('../databases/project');
var Projects = mongoose.model('projectModel');
var uuid = require('uuid/v4');
/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/projects',(req,res,next)=>{
  var data = [];
  Projects.find((err,projects)=>{

    for(let i = 0; i< projects.length; i++){
      data.push(projects[i]);
    }
    res.send(data).end();
  })

});
router.post('/projects',(req,res,next)=>{
  var data = [];
  Projects.find((err,projects)=>{

    for(let i = 0; i< projects.length; i++){
      data.push(projects[i]);
    }
    res.send(data).end();
  })

});
router.post('/project',(req,res,next)=>{
  var data = req.body;
  try{
    var p = new Projects();
    var now = new Date().getTime();
    p.id = uuid();
    p.name = data.name? data.name : "N/A";
    p.detail = data.detail? data.detail : "N/A";
    p.status = data.status? data.status : 0;
    p.startAt = data.starAt? data.starAt : now;
    p.lastEdit = now;
    p.dueAt = data.dueAt? data.dueAt : now;
    p.endAt = data.endAt? data.endAt : now;
    p.tasks = [];
    p.comments = [];
    p.save();
    res.status(200).end();
  }catch(err){
    console.log(err);
  }
});
router.post('/project/update',(req,res,next)=>{
  var data = req.body;
  Projects.find({id:data.id},(err,project)=>{
    try{
      if(project){
        let now = new Date().getTime();

        project[0].name = data.name? data.name : project[0].name;
        project[0].detail = data.detail? data.detail : project[0].detail;
        project[0].status = data.status? data.status : project[0].status;
        project[0].startAt = data.starAt? data.starAt : project[0].startAt;
        project[0].lastEdit = now;
        project[0].dueAt = data.dueAt? data.dueAt : project[0].dueAt;
        project[0].endAt = data.endAt? data.endAt : project[0].endAt;

        project[0].save();
        res.status(200).end();
      }
    }catch(err){
      console.log(err);
    }
    res.status(201).end();
  });
});

router.post('/project/add/task',(req,res,next)=>{
  let data = req.body;
  Projects.find({id:data.id},(err,project)=>{
    if(err){res.status(401).end()}
    if(project){
      project[0].tasks.push(data);
      project[0].save();
      res.status(200).end();
    }
    else{
        res.status(201).end();
    }

  })
});

router.post('/project/add/comment',(req,res,next)=>{
  let data = req.body;
  Projects.find({id:data.id},(err,project)=>{
    if(err){res.status(401).end()}
    if(project){
      project[0].comments.push(data);
      project[0].save();
      res.status(200).end();
    }
    else{
        res.status(201).end();
    }

  })
})

router.post('/project/remove/task',(req,res,next)=>{
  let data = req.body;
  Projects.find({id:data.id},(err,project)=>{
    if(err){res.status(401).end();}
    if(project){
      let tasks = project[0].tasks;
      for(let i = 0; i< tasks.length; i++){
        if(tasks[i].task == data.task){
          tasks.splice(i,1);
        }
      }
      project[0].tasks = tasks;
      project[0].save();
      res.status(200).end();
    }
    else{
      res.status(201).end();
    }
  });
});

router.post('/project/remove/comment',(req,res,next)=>{
  let data = req.body;
  Projects.find({id:data.id},(err,project)=>{
    if(err){res.status(401).end();}
    if(project){
      let comments = project[0].comments;
      for(let i = 0; i< comments.length; i++){
        if(comments[i].comment == data.comment){
          comments.splice(i,1);
        }
      }
      project[0].comment = comments;
      project[0].save();
      res.status(200).end();
    }
    else{
      res.status(201).end();
    }
  });
});

router.post('/connectionCheck',(req,res,next)=>{
  res.status(200).end();
})

router.get('/project/delete/:id',(req,res,next)=>{

  Projects.find({id:req.params.id},(err,data)=>{
      if(err){  res.status(501).end();  }
      if(data){
          for(let i = 0; i < data.length; i++){
              data[i].remove();
          }

      }
      res.status(200).end();
  });

  res.status(200).end();
});

router.post('/project/delete',(req,res,next)=>{
  let data = req.body;
  Projects.find({id:data.id},(err,data)=>{
      if(err){  res.status(501).end();  }
      if(data){
          for(let i = 0; i < data.length; i++){
              data[i].remove();
          }

      }
      res.status(200).end();
  });

  res.status(200).end();
})
module.exports = router;
