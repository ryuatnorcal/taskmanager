/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,

} from 'react-native';
import {Provider} from 'react-redux';
import Navigation from './src/middlewares/navigation';

import store from './src/middlewares/store';
import { COLOR, ThemeProvider} from 'react-native-material-ui';
const uiTheme = {
    palette: {
        primaryColor: COLOR.purple800,
        accentColor: COLOR.pink500,
    },
};

type Props = {};
export default class App extends Component<Props> {
  render() {
      return (
        <Provider store={store}>
          <ThemeProvider uiTheme={uiTheme}>
            <Navigation />
          </ThemeProvider>
        </Provider>
      );
   }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  navbar:{
    flex: 1,
    backgroundColor:"#6033cc",
    height:50
  }

});
