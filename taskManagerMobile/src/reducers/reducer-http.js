export default function(state=null,action){
  switch(action.type){
    case "SET_HTTP_RESPONSE":
        return {...state, res:action.payload,isResponed:true};
      break;
    case "TOGGLE_RESPONDED_PROP":
      return {...state, res:action.payload,isResponed:!state.isResponed};
      break;
    default:
      if(state){
        return state;
      }
      else{
        return {
            res: {},
            isResponed:false
          };
      }
      break;

  }
}
