

export default function(state=null,action){
  switch(action.type){
    case "SELECTED_PROJECT":
    case "REDEFINE_SELECTED_PROJECT":
      return action.payload;
      break;
    case "UNSELECT_PROJECT":
      return null;
      break;
    case "ADD_TASK":
      return  insertTask(state, action.payload);
      break;
    case "REMOVE_TASK":
      return removeTask(state,action.payload);
      break;
    case "ADD_COMMENT":
      return insertComment(state, action.payload);
      break;
    case "REMOVE_COMMENT":
      return removeComment(state, action.payload);
      break;
  }

  return state;
}
export const insertTask = (state,task)=>{
  state.tasks.push(task);
  return state;
}
// todo use task id when it removes
export const removeTask = (state,task)=> {
  for(let i = 0; i < state.tasks.length; i++){
    if(state.tasks[i].task == task.task){
      state.tasks.splice(i,1);
    }
  }
  return state;
}

export const insertComment = (state, comment) => {
  state.comments.push(comment);
  return state;
}

export const removeComment = (state, comment) => {
  for(let i = 0; i < state.comments.length; i++){
    if(state.comments[i].id == comment.id){
      state.comments.splice(i,1);
    }
  }
  return state;
}
