export default function(state=null,action){

  switch (action.type) {
    case "REDEFINE_THEMESETTING":
      return action.payload;
      break;
    case "EDIT_PROJECT":
    case "CREATE_NEW_PROJECT":
      console.log("create new project");
        return Object.assign({},state,{
          isProjectList:false,
          isProjectDetail:false,
          isProjectForm:true
        });
      break;
    case "SHOW_PROJECT_DETAIL":
        return Object.assign({},state,{
          isProjectList:false,
          isProjectDetail:true,
          isProjectForm:false
        });
      break;
    case "SETTING_INIT":
    case "SET_AS_DEFAULT":
      console.log("default ");
        return {
          isProjectList: true,
          isProjectDetail:false,
          isProjectForm: false
        };
      break;
  }
  return state;
}
