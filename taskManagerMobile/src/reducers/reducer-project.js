
export default function(state:Object=null,action:Object){
  switch(action.type){
    case "RECIEVED_PROJECTS":
    case "REDEFINE_PROJECTS":
    case "SET_PROJECTS":
      return action.payload;
      break;
    case "ADD_PROJECT":
      return [...state,action.payload];
      break;
    case "REMOVE_PROJECT":
      //return state.filter(({ id }) => id !== action.payload);
      console.log(state);
      return removeProject(action.payload.id, state);
      break;
    case "UPDATE_PROJECT":
      return updateData(action.payload,state);
      break;
    case "GET_PROJECTS_STATE":
      return state;
    default:
      if(state){
        return state;
      }
      else{
        return [];
      }
      break;
  }

}
export const updateData = (data:Object,state:Object) =>{

  for(let i = 0; i< state.length; i++){
    if(data.id == state[i].id){
      state[i] = data;
    }
  }
  return [...state];
}

const removeProject = (id:String, state:Object) => {
  for(let i = 0 ; i < state.length; i++){
    if(state[i].id == id){
      state.splice(i,1);
    }
  }
  return state;
}
