export default function(state=null,action){
  switch(action.type){
    case "SET_CONNECTION":
        return {...state, ip: action.payload.ip, port:action.payload.port};
      break;
    case "SKIP_CONNECTION_CONFIG":
        return {...state,allowNetwork:false};
      break;
    default:
      if(state){
        return state;
      }
      else{
        return {
            ip:"",
            port: "3001",
            allowNetwork: true
          };
      }
      break;

  }
}
