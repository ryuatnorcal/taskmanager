import {combineReducers} from 'redux';
import ProjectReducer from './reducer-project';
import ActiveProject from './reducer-active-project';
import StatusReducer from './reducer-status';
import MenuReducer from './reducer-menus';
import NavigationReducer from './reducer-navigation';
import IPConfigReducer from './reducer-ipconfig';
import HTTPReducer from './reducer-http';
const allReducers = combineReducers({
  projects: ProjectReducer,
  activeProject:ActiveProject,
  status: StatusReducer,
  menus: MenuReducer,
  nav: NavigationReducer,
  ipconfig: IPConfigReducer,
  http:HTTPReducer
});
export default allReducers;
