export default function(state=null,action){
  switch(action.type){
    default:
      return [
        {
          name: "Dashboard",
          icon: "home",
          view: "screen1"
        },
        {
          name: "Projects",
          icon: "note",
          view: "screen2"
        },
        {
          name: "Add Project",
          icon: "note add",
          view: "projectForm"
        }
      ]
  }
}
