import {NavigationAction,createStore,StackNavigator} from 'react-navigation';
import AppNavigator from '../routes';


const initialState = AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams('homeStack'));
export default (state:Object = initialState,action:Object) => {
  const nextState = AppNavigator.router.getStateForAction(action, state);
  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}
