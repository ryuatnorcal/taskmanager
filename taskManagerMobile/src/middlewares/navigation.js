import React from 'react';
import {addNavigationHelpers} from 'react-navigation';
import {createReduxBoundAddListener} from 'react-navigation-redux-helpers';
import { connect } from 'react-redux';
import Navigator from '../routes';

const addListener = ()=>{return createReduxBoundAddListener("root");}

class Nav extends React.Component {

  render() {    
    return (
      <Navigator navigation={addNavigationHelpers({
        dispatch: this.props.dispatch,
        state: this.props.nav,
        addListener
      })} />
    );
  }
}

const mapStateToProps = (state) => ({
  nav: state.nav
});

export default connect(mapStateToProps)(Nav);
