import {createStore,applyMiddleware} from 'redux';
import {
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';

import thunk from 'redux-thunk';
import promise from 'redux-promise-middleware';
import allReducers from '../reducers/index';

const NavigationMiddleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);

const middleware = applyMiddleware(promise(),thunk,NavigationMiddleware);
const store = createStore(allReducers,middleware);

export default store;
