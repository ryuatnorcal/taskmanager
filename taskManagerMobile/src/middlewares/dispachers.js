import {
  INIT_MENU,
  SETTING_INIT,
  SELECTED_PROJECT,
  ADD_TASK,
  REMOVE_TASK,
  UPDATE_TASK_IN_PORJECT,
  ADD_COMMENT,
  REMOVE_COMMENT,
  SET_CONNECTION,
  SET_PROJECTS,
  ADD_PROJECT,
  REMOVE_PROJECT,
  SKIP_CONNECTION_CONFIG,
  GET_PROJECTS_STATE

} from '../actions/types-action';
import {toast} from "../tools/tool";

export const getProjects = (ipconfig:Object, dispatch:Function) => {
  let url = generateURL(ipconfig, "/api/projects");
  postRequestWithRespond(url,{},dispatch,"SET_PROJECTS");
}

export const submitProjectToServer = (data:Object, ipconfig:Object, dispatch: Function) => {
  let url = generateURL(ipconfig, "/api/project");
  postRequestWithoutRespond(url,data);
}

export const addTaskToServer = (data:Object, ipconfig:Object, dispatch:Function, activeProject: Object  ) => {
  let url = generateURL(ipconfig, "/api/project/add/task");
  postRequestWithoutRespond(url,data);
}

export const removeTaskFromServer = (task:Object, ipconfig:Object, dispatch:Function) =>{
    let url = generateURL(ipconfig, "/api/project/remove/task");
    postRequestWithoutRespond(url,task);
}

export const addCommentToServer = (comment:Object, ipconfig: Object, dispatch: Function, activeProject: Object ) => {
  let url = generateURL(ipconfig, "/api/project/add/comment");
  postRequestWithoutRespond(url,comment);
}

export const removeCommentFromServer = (comment:Object, ipconfig:Object, dispatch:Function) => {
  let url = generateURL(ipconfig,"/api/project/remove/task");
  postRequestWithoutRespond(url,comment);
}

export const submitProjectToLocal = (data: Object,dispatch:Function) => {
  dispatch({type:ADD_PROJECT,payload:data});
}

export const removeProject = (data: Object, dispatch:Function) => {
  dispatch({type:REMOVE_PROJECT,payload:data});  
}

export const removeProjectFromServer = (data:Object, ipconfig:Object, dispatch:Function ) => {
  let url = generateURL(ipconfig,"/api/project/delete");
  postRequestWithoutRespond(url,data);
}

export const setActiveProject = (data:Object,dispatch:Function) => {
  dispatch({type: SELECTED_PROJECT,payload: data});
}

export const addTask = (task:Object, dispatch: Function, activeProject:Object) => {
  dispatch({type: ADD_TASK, payload: task});
}

export const removeTask = (task:Object, dispatch: Function) => {
  dispatch({type: REMOVE_TASK, payload: task});
}

export const addComment = (comment:Object, dispatch: Function) => {
  dispatch({type: ADD_COMMENT, payload: comment});
}

export const removeComment = (comment:Object, dispatch: Function) => {
  dispatch({type: REMOVE_COMMENT, payload: comment});
}

export const setConnection = (connection: Object, dispatch: Function) => {
  dispatch({type:SET_CONNECTION, payload:connection});
}
export const skipConnectionConfig = (connection:Object, dispatch: Function) => {
  dispatch({type:SKIP_CONNECTION_CONFIG,payload: connection});

}
const generateURL = (ipconfig:Object, postFix: String) => {
  return "http://" + ipconfig.ip + ":" + ipconfig.port + postFix;
}

const postRequestWithoutRespond = (url: String, data:Object) => {
  fetch(url,{
    method:"POST",
    headers:{
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data)
  }).then((response) => response.json()).then((data:Object)=>{

  }).catch((err)=>{
    toast("CATCH: SOMETHING WENT WRONG","SHORT " + err );
  });
}

const postRequestWithRespond = (url:String, data:Object, dispatch:Function, type: String) => {
  fetch(url,{
    method:"POST",
    headers:{
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body:JSON.stringify(data)
  }).then((response) => response.json()).then((res:Object)=>{
    dispatch({type: type,payload:res});
  }).catch((err)=>{
    toast("CATCH: SOMETHING WENT WRONG","SHORT " + err );
  });
}
