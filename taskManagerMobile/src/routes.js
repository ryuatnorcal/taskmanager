import React from 'react'
import { Text, Animated, Easing } from 'react-native';
import { IconToggle } from 'react-native-material-ui';
import { StackNavigator,DrawerNavigator } from 'react-navigation';
import HomeScreen from './components/home';
import PageScreen from './components/page';
import Screen1 from './components/screen1';
import Screen2 from './components/screen2';
import Screen3 from './components/screen3';
import DrawerContainer from './containers/container-drawer';
import projectForm from './components/form-project';
import ipconfigScreen from './components/form-ip';
import connectionCheckerScreen from './components/connectionChecker';
const noTransitionConfig = () => ({
  transitionSpec: {
    duration: 0,
    timing: Animated.timing,
    easing: Easing.step0
  }
})


const DrawerStack = DrawerNavigator({
  screen1: { screen: Screen1 },
  screen2: { screen: Screen2 },
  screen3: { screen: Screen3 },
  projectForm: { screen: projectForm },
}, {
  gesturesEnabled: false,
  contentComponent: DrawerContainer,

})

const drawerButton = (navigation) => {
  return (<IconToggle
    name="menu"
    style={{icon:{padding: 5, color: 'white'}}}
    onPress={() => {
      if (navigation.state.index === 0) {
        navigation.navigate('DrawerOpen');
      } else {
        navigation.navigate('DrawerClose');
      }
    }
  } />)
}

  const DrawerNavigation = StackNavigator({
    DrawerStack: { screen: DrawerStack }
  }, {
    headerMode: 'float',
    navigationOptions: ({navigation}) => ({
      headerStyle: {backgroundColor: '#6033cc'},
      headerTintColor: 'white',
      gesturesEnabled: false,
      headerLeft: drawerButton(navigation)
    })
  })

  const HomeStack = StackNavigator({
    homeScreen: { screen: ipconfigScreen },
    connectionScreen: {screen: connectionCheckerScreen}
  }, {
    headerMode: 'float',
    navigationOptions: {
      headerStyle: {backgroundColor: '#6033cc'},
      headerTintColor: 'white',

    }
  })

  const PrimaryNav = StackNavigator({
    homeStack: { screen: HomeStack },
    drawerStack: { screen: DrawerNavigation }
  }, {
    headerMode: 'none',
    title: 'Main',
    initialRouteName: 'homeStack',
    transitionConfig: noTransitionConfig
  })

  export default PrimaryNav;
