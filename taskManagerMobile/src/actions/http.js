import {SET_HTTP_RESPONSE,DEFAULT_IP, DEFAULT_PORT,TOGGLE_RESPONDED_PROP} from './types-action';

export const connectionCheck = (ipconfig:Object,dispatch: Function) => {
  if(!ipconfig.ip){ipconfig.ip = "192.168.128.31"; }
  if(!ipconfig.port){ipconfig.port = "3001"; }
  let url = "http://"+ ipconfig.ip + ":" + ipconfig.port + "/api/connectionCheck";
  return fetch(url,{
    method:"POST"
  }).then((respond) => errorHandling(respond,dispatch)).catch(()=>{
      dispatch({type: SET_HTTP_RESPONSE, payload:{}});
  });
}

const errorHandling = (res:Object,dispatch:Function) => {
  dispatch({type: SET_HTTP_RESPONSE, payload:res});
  return res;
}

export const changeConnectionRespondProp = (dispatch:Function) => {
  dispatch({type: TOGGLE_RESPONDED_PROP});
}

// POST Method
export const post = (data:Object, url: String, dispatch:Function) =>{
  return fetch(url,{
    method:"POST",
    data: data
  }).then(
    
  ).catch(

  );
}
// GET Method
