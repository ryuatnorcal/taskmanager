// INIT
export const INIT_MENU          = "INIT_MENU";
export const SETTING_INIT       = "SETTING_INIT";
export const GET_PROJECTS_STATE = "GET_PROJECTS_STATE";

// ADD
export const ADD_PROJECT        = "ADD_PROJECT";
export const ADD_TASK           = "ADD_TASK";
export const ADD_COMMENT        = "ADD_COMMENT";

// SET
export const SET_IP             = "SET_IP";
export const SET_PORT           = "SET_PORT";
export const SET_CONNECTION     = "SET_CONNECTION";
export const SET_HTTP_RESPONSE  = "SET_HTTP_RESPONSE";
export const SET_PROJECTS       = "SET_PROJECTS";

// SELECT
export const SELECTED_PROJECT   = "SELECTED_PROJECT";

// UNSELECT
export const UNSELECT_PROJECT   = "UNSELECT_PROJECT";

//REMOVE
export const REMOVE_TASK        = "REMOVE_TASK";
export const REMOVE_COMMENT     = "REMOVE_COMMENT";
export const REMOVE_PROJECT     = "REMOVE_PROJECT";

// UPDATE
export const UPDATE_TASK_IN_PORJECT = "UPDATE_TASK_IN_PORJECT";
export const REDEFINE_SELECTED_PROJECT = "REDEFINE_SELECTED_PROJECT";

// OTHERS
export const TOGGLE_RESPONDED_PROP  = "TOGGLE_RESPONDED_PROP";
export const SKIP_CONNECTION_CONFIG = "SKIP_CONNECTION_CONFIG";
// VARIABLES
export const DEFAULT_IP         = "192.168.128.31";
export const DEFAULT_PORT       = "3001";
export const CONNECTION_ERROR_MSG = "Could not connect to server, Please retry again";
export const LONG               = "LONG";
export const SHORT              = "SHORT";
