import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { NavigationActions } from 'react-navigation';
import {connect} from 'react-redux';
import { ListItem } from 'react-native-material-ui';
class DrawerContainer extends React.Component {

  menuItems = () => {
    return this.props.menus.map((menu)=>{
      return(
        <View key={menu.name}>
          <ListItem
            centerElement={{
              primaryText: menu.name,
            }}
            leftElement={ menu.icon }
            onPress={()=>this.navigationHandler(menu.view)}
          />
        </View>
      )
    });
  }

  navigationHandler = (view: String) => {
    this.props.navigation.navigate(view);
    return false;
  }



  render() {
    return (
      <View>
        {this.menuItems()}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f6f6f6',
    paddingTop: 40,
    paddingHorizontal: 20
  },
  uglyDrawerItem: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#E73536',
    padding: 15,
    margin: 5,
    borderRadius: 2,
    borderColor: '#E73536',
    borderWidth: 1,
    textAlign: 'center'
  }
})

function mapStateToProps(state){
  return {
    menus:state.menus,
    ipconfig: state.ipconfig,
    http: state.http
  }
}


export default connect(mapStateToProps)(DrawerContainer);
