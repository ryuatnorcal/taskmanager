/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,

} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Toolbar, Button } from 'react-native-material-ui';

type Props = {};
class Screen1 extends Component<Props> {

  getStats = () => {
    let projectCount = this.props.projects.length;
    if(projectCount > 0){
      return(
        <View>
          <Text style={styles.text}>You have {projectCount} projects.</Text>
          <Button
            primary
            text="View Projects"
            raised="true"
            onPress={()=>this.props.navigation.navigate('screen2')}
            style={{container:{width: 150}}}/>
        </View>
      )
    }
    else{
      return(
        <View>
          <Text style={styles.text}>You have No Project</Text>
          <Button
            primary
            text="CREATE PROJECT"
            raised="true"
            onPress={()=>this.props.navigation.navigate('projectForm')}
            style={{container:{width: 150}}}/>
        </View>
      )
    }
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View>
          <Text style={styles.headerText}>Project Manager</Text>
        </View>
        <View style={styles.subContainer}>
            {this.getStats()}
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex:1,
    padding:10
  },
  headerText:{
    fontSize: 20,
    fontWeight: "500",
    marginBottom: 50
  },
  subContainer: {
    marginRight: "auto",
    marginLeft: "auto"
  },
  text:{
    marginBottom:10,
    textAlign:"center"
  }
});

function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
    themeSetting: state.themeSetting,
    status: state.status,
    ipconfig: state.ipconfig,
    http: state.http
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}
export default connect(mapStateToProps)(Screen1);
