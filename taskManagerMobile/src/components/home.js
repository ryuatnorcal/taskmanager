/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Toolbar } from 'react-native-material-ui';

type Props = {};
class Home extends Component<Props> {

  render() {
    console.log(this.props.navigation);
    return (
      <View>
        <Text>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like). </Text>
      </View>
    );
  }
}

function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
    themeSetting: state.themeSetting,
    status: state.status,
    nav:state.nav
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}
export default connect(mapStateToProps)(Home);
