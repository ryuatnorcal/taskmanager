/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Toolbar, Button } from 'react-native-material-ui';
import {connectionCheck } from '../actions/http';
import {toast} from '../tools/tool';
import {CONNECTION_ERROR_MSG,LONG} from '../actions/types-action';



type Props = {};
class ConnectionChecker extends Component<Props> {
  componentDidMount = () => {
    let connection = connectionCheck(this.props.ipconfig,this.props.dispatch);    
    setTimeout(()=>this.loading(),3000);
  }

  loading = () =>{
    if(this.props.http && this.props.http.isResponed){
      if(this.props.http.res.ok){

        this.props.navigation.navigate('drawerStack');
      }
      else{
        toast(CONNECTION_ERROR_MSG,LONG);
        this.props.navigation.navigate("homeScreen");
      }
    }
    return;
  }

  render() {
    return (
      <View>
        <Text>Connection Checking</Text>
        <Text>IP: {this.props.ipconfig.ip}</Text>
        <Text>Port: {this.props.ipconfig.port}</Text>
        <Text>isResponed: {this.props.http.isResponed}</Text>

      </View>
    );
  }
}

function mapStateToProps(state){
  return {
    ipconfig: state.ipconfig,
    http: state.http
  }
}

export default connect(mapStateToProps)(ConnectionChecker);
