/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Alert
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { ListItem } from 'react-native-material-ui';
import FormTask from './form-task';
import FormComment from './form-comment';
import {removeTask,removeComment,removeTaskFromServer,removeCommentFromServer} from '../middlewares/dispachers';
type Props = {};
class Screen3 extends Component<Props> {

  removeCommentHandler = (comment:Object) => {
    Alert.alert(
      'Confirm Deletion',
      'Would you like to delete '+ comment.text +'?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => this.removeComment(comment)},
      ]
    )
  }
  removeTaskHandler = (task:Object) => {
    Alert.alert(
      'Confirm Deletion',
      'Have you done task '+ task.task +'?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => this.removeTask(task)},
      ]

    )
  }
  removeComment = (comment) => {
    removeComment(comment,this.props.dispatch);
    if(this.props.ipconfig.allowNetwork){
      removeCommentFromServer(comment,this.props.ipconfig,this.props.dispatch);
    }
  }
  removeTask = (task) => {
    removeTask(task,this.props.dispatch);
    if(this.props.ipconfig.allowNetwork){
      removeTaskFromServer(task,this.props.ipconfig,this.props.dispatch);
    }
  }


 // todo Key to taskID
  getTasks = () => {
    if(this.props.activeProject.tasks.length > 0){
      return this.props.activeProject.tasks.map((task)=>{
        return (
          <View key={task.task}>
            <ListItem
              centerElement={{
                primaryText: task.task,
              }}
              onLongPress={() => this.removeTaskHandler(task) }
            />
          </View>
        )
      })
    }
    else {
      return (<Text style={styles.fallBackText}>There is no task </Text>);
    }
  }

  // todo: key to commentID
  getComments = () => {
    if(this.props.activeProject.comments.length > 0){
      return this.props.activeProject.comments.map((comment)=>{
        return (
          <View key={comment.text}>
            <ListItem
              centerElement={{
                primaryText: comment.text,
              }}
              onLongPress={() => this.removeCommentHandler(comment) }
            />
          </View>
        )
      })
    }
    else {
      return (<Text style={styles.fallBackText}>There is no comment </Text>);
    }
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View>
          <Text style={styles.header}>{this.props.activeProject.name}</Text>
        </View>
        <View style={styles.views }>
          <Text>Start: {this.props.activeProject.startAt}</Text>
          <Text>Due: {this.props.activeProject.dueAt}</Text>
          <Text>End: {this.props.activeProject.endAt}</Text>
        </View>
        <View style={styles.views }>
          <Text>{this.props.activeProject.detail}</Text>
        </View>
        <View style={styles.views }>
          <Text style={styles.subHeader}>Tasks</Text>
          {this.getTasks()}
          <FormTask />
        </View>
        <View style={styles.views }>
          <Text style={styles.subHeader}>Comment</Text>
          {this.getComments()}
          <FormComment />
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    padding:10,
    backgroundColor:"#FFF"
  },
  header: {
    fontWeight: "700",
    fontSize: 20,
    paddingBottom: 10
  },
  subHeader:{
    fontWeight: "500",
    fontSize: 15,
  },
  views:{
    paddingBottom: 10
  },
  fallBackText:{
    textAlign: "center"
  }

});

function mapStateToProps(state){
  return {
    activeProject: state.activeProject,
    status: state.status,
    ipconfig:state.ipconfig
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}
export default connect(mapStateToProps)(Screen3);
