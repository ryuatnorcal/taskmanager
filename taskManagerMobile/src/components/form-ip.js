/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Button } from 'react-native-material-ui';
import {setConnection,skipConnectionConfig} from '../middlewares/dispachers';
type Props = {};
class FormIp extends Component<Props> {


  constructor(){
    super();
    this.state = {};
  }

  inputHandler = (text:String, type: String) => {
    switch(type){
      case "IP":
        this.setState({
          ip: text
        });
        break;
      case "PORT":
        this.setState({
          port: text
        });
        break;
    }

  }

  submit = () => {
    if(!this.state.port){
      this.setState({
        port: "3001"
      })

    }
    setConnection(this.state,this.props.dispatch);

    this.props.navigation.navigate("connectionScreen");
  }
  skip = () => {
    skipConnectionConfig({allowNetwork:false},this.props.dispatch);
    this.props.navigation.navigate('drawerStack');

  }

  render() {
    return (
      <ScrollView style={styles.container}>
          <View style={styles.body}>
            <Text style={styles.header}>IP Configuration</Text>
            <Text style={styles.views}>This will enable to connect with local server. Please connect the mobile device with the same network with taskManager-server is running.
            Please start mongoDB before running Express server.
            </Text>
            <Text style={styles.views}>More information will be available at READ.me or contact to me</Text>

            <Text style={styles.subHeader}>Server IP</Text>
            <TextInput
              onChangeText={(text) => this.inputHandler(text, "IP")}
              />
            <Text style={styles.subHeader}>Port (optional)</Text>
            <TextInput
              onChangeText={(text) => this.inputHandler(text, "PORT")}
              />
            <Button primary text="Set" onPress={()=>this.submit()} />
          </View>
          <View style={styles.footer}>
            <Text>Selecting SKIP disable the Network connection. However, you will see sample data set on the app</Text>
            <Button primary text="Skip" onPress={()=>this.skip()} />
          </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding:10,
    backgroundColor:"#FFF",
    flex:1
  },
  header: {
    fontWeight: "700",
    fontSize: 20,
    paddingBottom: 10
  },
  subHeader:{
    fontWeight: "500",
    fontSize: 15,
  },
  views:{
    paddingBottom: 10
  },
  fallBackText:{
    textAlign: "center"
  },
  body:{
    flex:3
  },
  footer: {
    flex:1
  }

});

function mapStateToProps(state){
  return {
    ipconfig: state.ipconfig
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}
export default connect(mapStateToProps)(FormIp);
