/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Toolbar } from 'react-native-material-ui';




type Props = {};
class Home extends Component<Props> {
  hello = () => {
    // show the slide here
    alert("hello");
  }
  render() {
    return (
      <View>
        <Text>Page Screen </Text>
      </View>
    );
  }
}

function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
    themeSetting: state.themeSetting,
    status: state.status
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}
export default connect(mapStateToProps)(Home);
