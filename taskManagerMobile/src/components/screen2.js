/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ScrollView,
  Alert
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Card,Button } from 'react-native-material-ui';
import {setActiveProject,getProjects,removeProject,removeProjectFromServer} from '../middlewares/dispachers';
import {timestampToDate} from "../tools/tool";
type Props = {};
class Screen2 extends Component<Props> {

  renderView = (arg:Object) => {
    setActiveProject(arg,this.props.dispatch);
    this.props.navigation.navigate('screen3');
  }
  componentDidMount = () =>{
    if(this.props.ipconfig.allowNetwork){
      getProjects(this.props.ipconfig, this.props.dispatch);
    }
  }
  removeProjectHandler = (project: Object) => {
    Alert.alert(
      'Confirm Deletion',
      'Would you like to delete '+ project.name +'?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () => this.removeProject(project)},
      ]
    )
  }

  removeProject = (arg:Object) => {
    if(arg){
      removeProject(arg,this.props.dispatch);
      if(this.props.ipconfig.allowNetwork){
          removeProjectFromServer(arg, this.props.ipconfig,this.props.dispatch);
      }
      this.props.navigation.navigate('screen2');
    }

  }

  listOfCards = () => {
    if(this.props.projects.length > 0){
      return this.props.projects.map((project)=>{
        return(
          <View key={project.id}>
            <Card>
              <View style={styles.cardHeader}>
                <Text style={styles.cardHeaderText}>{project.name.toUpperCase()}</Text>
              </View>
              <View style={styles.cardBody}>
                <Text>Start: {timestampToDate(project.startAt)}</Text>
                <Text>Due: {timestampToDate(project.dueAt)}</Text>
                <Text>End: {timestampToDate(project.endAt)}</Text>
              </View>
              <View style={styles.cardBody}>
                <Text>{project.detail}</Text>
                <Text>{this.props.status[project.status].option}</Text>
              </View>
              <View style={styles.cardFooter}>
                <Button
                  primary
                  text="Delete"
                  style={{container:{flex:1}}}
                  onPress={()=> this.removeProjectHandler(project)}/>
                <Button
                  primary
                  text="View"
                  style={{container:{flex:1}}}
                  onPress={()=>this.renderView(project)}/>
              </View>
            </Card>
          </View>
        )
      });
    }
    else{
      return(
        <View style={{marginLeft:"auto", marginRight:"auto"}}>
          <Text style={{textAlign:"center", paddingTop:10, paddingBottom:10}}>There is no projects</Text>
          <Button
            primary
            text="CREATE PROJECT"
            raised="true"
            onPress={()=>this.props.navigation.navigate('projectForm')}
            style={{container:{width: 150}}}/>
        </View>
      )
    }

  }
  render() {

    return (
      <ScrollView>
        {this.listOfCards()}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({

  cardHeader:{
    padding:10

  },
  cardHeaderText: {
    fontWeight:"500",
    fontSize: 20
  },
  cardBody:{
    paddingLeft: 10,
    paddingRight:10,
    paddingBottom:10
  },
  cardFooter:{
    height:50,
    flexDirection:"row",
    flex:1
  }
});


function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
    status: state.status,
    ipconfig: state.ipconfig
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}
export default connect(mapStateToProps)(Screen2);
