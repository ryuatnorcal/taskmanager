/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Toolbar, Button } from 'react-native-material-ui';
import uuid from 'uuid/v4';
import {addTask,addTaskToServer} from '../middlewares/dispachers';

type Props = {};
class FormTask extends Component<Props> {
  constructor(){
    super();
    this.state = {};
  }

  componentWillMount = () => {
    this.initializeState();
  }

  inputHandler = (text) => {
    this.setState({
      tasks:{
        id: this.props.activeProject.id,
        task : text
      }
    })
  }

  submit = () => {
    if(this.state.tasks.task){
        addTask(this.state.tasks,this.props.dispatch, this.props.activeProject);
        if(this.props.ipconfig.allowNetwork){
          addTaskToServer(this.state.tasks,this.props.ipconfig,this.props.dispatch,this.props.activeProject);
        }
    }
    else{
      console.log("task undefined ");
    }
    this.initializeState();
  }

  initializeState = () => {
    this.setState({
      tasks: {
        id: this.props.activeProject.id,
        task: ""
      }
    })
  }

  render() {
    return (
      <View>
        <TextInput
          onChangeText={(text) => this.inputHandler(text)}
        />
        <Button primary text="Create Task" onPress={()=>this.submit()} />
      </View>
    );
  }
}

function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
    themeSetting: state.themeSetting,
    status: state.status,
    ipconfig: state.ipconfig
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}
export default connect(mapStateToProps)(FormTask);
