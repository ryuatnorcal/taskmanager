/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import { Toolbar } from 'react-native-material-ui';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' +
    'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class Navbar extends Component<Props> {
  hello = () => {
    // show the slide here
    alert("hello");
  }
  render() {
    return (

        <Toolbar
          leftElement="menu"                    
          centerElement="Project Manager"
          leftElement={[
            "menu"
          ]}
          onLeftElementPress={()=>this.props.openDrawer()}
        />

    );
  }
}

const styles = StyleSheet.create({
  navbar:{
    flex: 1,
    backgroundColor:"#6033cc",
    height:50
  }
});
