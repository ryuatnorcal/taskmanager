/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Toolbar,Button } from 'react-native-material-ui';
import {addComment,addCommentToServer} from '../middlewares/dispachers';
import uuid from 'uuid/v4';
type Props = {};
class FormComment extends Component<Props> {
  constructor(){
    super();
    this.state = {};
  }

  componentWillMount = () => {
    this.setState({
      comment: {
        text: "Enter your comment",
        time: "",
        id: ""
      }
    });
  }

  inputHandler = (text:String) => {
    let now = new Date();
    this.setState({
      comment:{
        text: text,
        id: this.props.activeProject.id,
        time: now        
      }
    })
  }

  submit = () => {

    if(this.state.comment.text){
        addComment(this.state.comment,this.props.dispatch, this.props.activeProject);
        if(this.props.ipconfig.allowNetwork){
          addCommentToServer(this.state.comment, this.props.ipconfig, this.props.dispatch, this.props.activeProject);
        }
    }
    else{
      console.log("task undefined ");
    }
  }

  render() {
    return (
      <View>
        <TextInput
          onChangeText={(text) => this.inputHandler(text)}
        />
        <Button primary text="Leave Comment" onPress={()=>this.submit()} />
      </View>
    );
  }
}

function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
    themeSetting: state.themeSetting,
    status: state.status,
    ipconfig: state.ipconfig
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}
export default connect(mapStateToProps)(FormComment);
