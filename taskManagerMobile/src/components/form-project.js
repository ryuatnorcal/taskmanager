/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView
} from 'react-native';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import { Toolbar, Button } from 'react-native-material-ui';
import uuid from 'uuid/v4';
import DateTimePicker from 'react-native-modal-datetime-picker';
import {addTask,submitProjectToServer,submitProjectToLocal} from '../middlewares/dispachers';

type Props = {};
class FormProject extends Component<Props> {
  constructor(){
    super();
    this.state = {};
  }

  componentWillMount = () => {
    let now = new Date();
    this.setState({
      project: {
        id: "",
        name: "",
        detail: "",
        status: 0,
        startAt: now.toLocaleDateString(),
        endAt: now.toLocaleDateString(),
        dueAt: now.toLocaleDateString(),
        comments: [],
        tasks: []
      },
      isDateTimePickerVisible: false,
      datePickerState: "",
    });
  }

  inputHandler = (text,type) => {
    
    switch(type){
      case "NAME" :
        this.state.project.name = text;
      break;
      case "DETAIL" :
        this.state.project.detail = text;
      break;
    }

  }

  submit = () => {
    this.state.project.id = uuid();
    console.log(this.state);
    if(this.props.ipconfig.allowNetwork){
      submitProjectToServer(this.state.project,this.props.ipconfig,this.state.dispatch);
    }
    else{
      submitProjectToLocal(this.state.project,this.props.dispatch);
    }
    this.props.navigation.navigate('screen2');

  }
  showDatePicker = (state) => {
    this.setState({
      isDateTimePickerVisible: true,
      datePickerState: state
    })
  }
  disableDatePicker = () => {
    this.setState({
      isDateTimePickerVisible: false,
    })
  }
  datePickerHandler = (date) => {
    switch(this.state.datePickerState){
      case "START":
        this.setState({
          project:{
            ...this.state.project,
            startAt: date.toLocaleDateString()
          },
          isDateTimePickerVisible: false
        })

        break;
      case "END":
        this.setState({
          project:{
            ...this.state.project,
            endAt: date.toLocaleDateString()
          },
          isDateTimePickerVisible: false
        })
        break;
      case "DUE":
        this.setState({
          project:{
            ...this.state.project,
            dueAt: date.toLocaleDateString()
          },
          isDateTimePickerVisible: false
        })
        break;
    }
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <View>
          <Text style={styles.headerText}> Create New Project</Text>
        </View>
        <View style={styles.formSections}>
          <Text>Project Name:</Text>
          <TextInput
            onChangeText={(text) => this.inputHandler(text,"NAME")}
          />
        </View>
        <View style={styles.formSections}>
          <Text>Detail:</Text>
          <TextInput
            onChangeText={(text) => this.inputHandler(text,"DETAIL")}
          />
        </View>
        <View style={styles.datePicker}>
          <View style={styles.datePickerChild}>
            <Text>Start:</Text>
            <Text onPress={()=>this.showDatePicker("START")}>{this.state.project.startAt}</Text>
          </View>
          <View style={styles.datePickerChild}>
            <Text>End:</Text>
            <Text onPress={()=>this.showDatePicker("END")}>{this.state.project.endAt}</Text>
          </View>
          <View style={styles.datePickerChild}>
            <Text>Due:</Text>
            <Text onPress={()=>this.showDatePicker("DUE")}>{this.state.project.dueAt}</Text>
          </View>
        </View>
        <View style={styles.footer}>
          <DateTimePicker
            isVisible={this.state.isDateTimePickerVisible}
            onConfirm={this.datePickerHandler}
            onCancel={this.disableDatePicker}
          />
          <Button primary text="Create Project" onPress={()=>this.submit()} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({

  container:{
    padding:5,
  },
  headerText: {
    fontWeight:"500",
    fontSize: 20,
    textAlign:"left"
  },
  formSections: {
    flex:1,
    paddingLeft:10,
    paddingRight:10
  },
  datePicker: {
    flex:1,
    flexDirection:"row",
    paddingLeft: 10,
    paddingRight: 10
  },
  datePickerChild: {
    flex:1
  },
  footer:{
    flex:1
  }

});

function mapStateToProps(state){
  return {
    projects: state.projects,
    activeProject: state.activeProject,
    themeSetting: state.themeSetting,
    status: state.status,
    ipconfig: state.ipconfig
  }
}

function matchDispatchToProps(dispatch){
  return bindActionCreators({selectProject:selectProject},dispatch)
}
export default connect(mapStateToProps)(FormProject);
