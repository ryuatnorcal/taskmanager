
import {ToastAndroid,Platform,AlertIOS} from 'react-native';
export const toast = (msg:String, length: String) => {
  if(Platform.OS === 'android'){
    switch(length){
      case "SHORT":
        ToastAndroid.show(msg,ToastAndroid.SHORT);
        break;
      case "LONG":
        ToastAndroid.show(msg,ToastAndroid.LONG);
        break;
    }
  }
  else{
    AlertIOS.alert("Error",[msg]);
  }
}
export const timestampToDate = (date) =>{
  if(date){
    let months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
    if(typeof date == 'string'){
      date = new Date(date);
    }
    return date.getDate() + " " + months[date.getMonth()]  + " " + date.getFullYear();
  }
  else{
    return "TBA";
  }

}
